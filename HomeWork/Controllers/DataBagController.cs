﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HomeWork.Controllers
{
    public class DataBagController : Controller
    {
        // GET: DataBag
        public ActionResult Index(int stuId,string stuName)
        {
            ViewBag.stuId = stuId;
            ViewData["stuName"] = stuName;
            return View();
        }
        public ActionResult FirstRout(int stuId, string stuName)
        {
            TempData.Add("stuId", stuId);
            TempData.Add("stuName", stuName);
            return RedirectToAction("SecondRout", TempData);
        }
        public ActionResult SecondRout()
        {
            return View();
        }

    }
}